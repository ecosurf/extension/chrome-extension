var template = "<div class=\"badge-circle\" style=\"background: ##COLOR##;\">##TEXT##</div>";

const FLOOR1=4;
const FLOOR2=6;

const RED="#ff0000";
const ORANGE="#ffa500";
const GREEN="#00ff00";

function _badge(color, index){
	return template.replace("##COLOR##", color).replace("##TEXT##", index);
}

async function getIndex(website) {
	let result = await fetch("https://intensif05.ensicaen.fr/consomanager/api/conso/website/1?url="+website,
	{
		method: 'GET',
		headers: {
		  'Content-Type': 'application/json',
		}
	});
	return result.json();
}

async function Badge(website) {
	let result = await getIndex(website)
	let index = result.conso.server_conso_data;
	let color;
	console.log(index);
	if (index < FLOOR1) {
		color = GREEN;
	} else if (index < FLOOR2) {
		color = ORANGE;
	} else {
		color = RED;
	}
	document.body.innerHTML = _badge(color, index);
}
