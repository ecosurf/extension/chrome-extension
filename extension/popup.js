document.onreadystatechange = () => {
    chrome.storage.local.get("co2").then(function(elt) {
        document.getElementById("co2").innerText = elt.co2.toFixed(2);
    });
    chrome.storage.local.get("co2_tot").then(function(elt) {
        console.log(elt);
        document.getElementById("co2_tot").innerText = elt.co2_tot.toFixed(2);
    });
}
