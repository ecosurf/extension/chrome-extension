
var index = 0;

const FLOOR1 = 4;
const FLOOR2 = 6;

function userAction(tab) {
    let basename = tab.url.split("/");
    fetch("https://intensif05.ensicaen.fr/consomanager/api/conso/website/1?url="+basename[2])
        .then(response => response.json())
        .then(text => worker_task(text))
        .catch(error => console.log(error))
    return true;  // Will respond asynchronously.
}

function collect() {
    fetch("https://intensif05.ensicaen.fr/usermanager/api/user/current?userId=1")
    .then(response => response.json())
    .then(text => {
        console.log(text);
        chrome.storage.local.set({"co2_tot": text.totalQuantityCO2})
    })
    .catch(error => console.log(error));
}


function getCurrentTab(callback) {
    let queryOptions = { active: true, lastFocusedWindow: true };
    chrome.tabs.query(queryOptions, ([tab]) => {
        if (chrome.runtime.lastError) console.error(chrome.runtime.lastError);
        callback(tab);
    });
}

function worker_task(index) {
    index = index.conso.server_conso_data;
    chrome.storage.local.set({"co2": index});
    index = index * 40;
    if (index < FLOOR1) {
        chrome.action.setIcon({ path: "./ecosurf_green.png"});
    } else if (index < FLOOR2) {
        chrome.action.setIcon({ path: "./ecosurf_orange.png" });
    } else {
        chrome.action.setIcon({ path: "./ecosurf_red.png" });
    }
    collect();
}

function task() {
    getCurrentTab(userAction);
}

chrome.tabs.onCreated.addListener(task);
chrome.tabs.onActivated.addListener(task);
chrome.tabs.onUpdated.addListener(task);

